package ec.krugercorp.hiberus.common.error;

public class HiberusError<E> implements IHiberusError<E> {
    private E code;
    private String message;
    private Integer index;

    public HiberusError(E code, String message, Integer index) {
        this.code = code;
        this.message = message;
        this.index = index;
    }

    public HiberusError(E code, String message) {
        this(code, message, null);
    }

    public HiberusError(E code) {
        this(code, null, null);
    }

    public HiberusError(String message) {
        this(null, message, null);
    }

    @Override
    public E getCode() {
        return code;
    }

    @Override
    public void setCode(E code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public Integer getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
