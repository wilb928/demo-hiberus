package ec.krugercorp.hiberus.domain.repository;

import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroCreateRequest;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroDetail;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroEditRequest;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroFilterRequest;

import java.util.List;

public interface ISuperHeroRepository {

    List<SuperHeroDetail> selectAll();
    List<SuperHeroDetail> selectByFilters(SuperHeroFilterRequest dto);
    SuperHeroDetail selectById(String id);
    String insert(SuperHeroCreateRequest dto);
    void update(SuperHeroEditRequest dto);
    void delete(String id);

}
