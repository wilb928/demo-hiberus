package ec.krugercorp.hiberus.infrastructure.inbound.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@SpringBootTest
@AutoConfigureMockMvc
class SuperHeroControllerTest extends Specification {

    @Autowired
    private MockMvc mock

    String baseEndpoint = "/api/v1/superheroes"

    def setupSpec() {
    }

    def cleanup() {
    }

    def "create"() {
        String request = new File("src/test/resources/request/", nombreJson).text

        when: "se consume un endpoint estructurado con los datos a probar"
        def response = mock.perform(post(baseEndpoint)
                .content(request)
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse()

        then: "se recibe la respuesta con el status esperado"
        assert response.status == status

        where: "cambiamos los valores"
        nombreJson                          | status
        "super-heroes-create.json"          | 201
        "not-valid.json"                    | 400
        "empty.json"                        | 500
    }
}
