package ec.krugercorp.hiberus.infrastructure.controller.response;

import ec.krugercorp.hiberus.common.error.HiberusError;
import ec.krugercorp.hiberus.domain.enums.ErrorType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@Builder
public class ErrorResponse {
    @Builder.Default
    private Date requestedDate = new Date();
    private Collection<HiberusError<ErrorType>> errors;
}
