package ec.krugercorp.hiberus.domain.enums;

import lombok.Getter;

@Getter
public enum StatusType {
    DELETED,//0
    DISABLED,//1
    ENABLED,//2
}
