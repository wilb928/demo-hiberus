package ec.krugercorp.hiberus.domain.errors;

import ec.krugercorp.hiberus.common.error.HiberusError;
import ec.krugercorp.hiberus.common.error.HiberusRuntimeException;
import ec.krugercorp.hiberus.domain.enums.ErrorType;

import java.util.ArrayList;
import java.util.Collection;
public class AppException extends HiberusRuntimeException {

    public static final String DEFAULT_MESSAGE = "HIBERUS => A ocurrido un error inesperado de la aplicación.";

    private Collection<HiberusError<ErrorType>> errors;

    public AppException(Collection<HiberusError<ErrorType>> errors, String message, Throwable causeException) {
        super(message, causeException);
        this.errors = errors;
    }

    public AppException(Collection<HiberusError<ErrorType>> errors, String message) {
        this(errors, message, null);
    }

    public AppException(Collection<HiberusError<ErrorType>> errors, Throwable causeException) {
        this(errors, DEFAULT_MESSAGE, causeException);
    }

    public AppException(Collection<HiberusError<ErrorType>> errors) {
        this(errors, DEFAULT_MESSAGE, null);
    }

    public AppException(Throwable causeException) {
        super(DEFAULT_MESSAGE, causeException);
    }

    public Collection<HiberusError<ErrorType>> getErrors() {
        return errors;
    }

}