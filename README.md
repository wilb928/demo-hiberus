# Demo Hiberus project

_Este es un proyecto de ejemplo para hiberus desarrollado con SpringBoot_

_Descripción del proyecto_

- Se tiene un ejemplo de un crud controller y sus respectivos test, **que deben ser eliminados una vez se comprenda la estructura del proyecto**
- Aplicación basada en **Java 11 Open JDK**
- Dependencias administradas con **Gradle 6.7**
- Análisis de test estático con **SonarQube 8.3.1 Community**
- Aplicación con arquitectura **hexagonal**

### 1. Swagger UI

Utilizando Swagger UI para exponer la documentación de nuestra API, podemos ahorrarnos mucho tiempo debido
a que podremos organizar nuestros métodos e incluso poner los ejemplos que necesitemos.

Dirección de swagger UI:

```
http://[IP_SERVER]:[PORT_SERVER]/swagger-ui.html#

Por defecto esta corriendo sobre esta url:

http://localhost:8080/swagger-ui.html
```
___


### 2. Ejecución

Con el siguiente comando se puede ejecutar el proyecto:

```
gradlew bootRun
```

### 3. Pruebas funcionales del servicio desplegado para la generación de proyectos
Actualmente el generador ya cuenta con una colección de casos de prueba implementadas en un cliente
postman con su respectivo entorno de variables postman que pueden ser importados y se encuentra dentro del directorio de este proyecto en la siguiente ubicación:

```
./others/postman/HIBERUS.postman_collection.json
./others/postman/Hiberus-LOCALHOST.postman_environment.json
```

### 4. Ejecución de pruebas de desarrollo
```
gradle unitTest
gradle integrationTest
gradle acceptanceTest

gradle allTest
```
### 5. Compilación y publicación para el SonarQube  
```
gradle --refresh-dependencies clean build test jacocoTestReport copyLombokToLibOut
sonar-scanner '-Dsonar.login=access-key'
```

### 6. Autores

* **Wilber Padilla Orellana** - *Desarrollador* - [wpadilla@bo.krugercorp.com](wpadilla@bo.krugercorp.com)
