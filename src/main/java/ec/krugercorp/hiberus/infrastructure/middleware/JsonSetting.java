package ec.krugercorp.hiberus.infrastructure.middleware;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JsonSetting {
    public static final Gson GSON = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
}
