package ec.krugercorp.hiberus.common.error;

public interface IHiberusError<E> {
    E getCode();
    void setCode(E code);
    String getMessage();
    void setMessage(String message);
    Integer getIndex();
    void setIndex(Integer index);
}
