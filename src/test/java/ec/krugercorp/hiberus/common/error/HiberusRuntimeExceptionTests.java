package ec.krugercorp.hiberus.common.error;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class HiberusRuntimeExceptionTests {    

    @Test
    void readAllMessages() {
        var expected = "mensage exception; \n";

        var result = HiberusRuntimeException.readAllMessages(new Exception("mensage exception"));

        assertThat(expected).isEqualTo(result);
    }
}
