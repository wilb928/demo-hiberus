package ec.krugercorp.hiberus.common.error;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class HiberusRuntimeException extends RuntimeException {

    public HiberusRuntimeException(String message) {
        super(message);
    }

    public HiberusRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public HiberusRuntimeException(Throwable cause) {
        super(cause);
    }

    public HiberusRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public static boolean hasChildException(Throwable ex) {
        return ex.getCause() != ex && ex.getCause() != null;
    }

    public boolean hasChildException() {
        return this.getCause() != this && this.getCause() != null;
    }

    public static void readAll(StringBuilder builder, Throwable ex) {
        boolean hasChildException = hasChildException(ex);
        builder.append("Class: ").append(ex.getClass()).append('\n');
        builder.append("HasCause: ").append(hasChildException).append('\n');
        builder.append("Message: ").append(ex.getMessage()).append('\n');
        builder.append("LocalizedMessage: ").append(ex.getLocalizedMessage()).append('\n');
        var traces = ex.getStackTrace();
        if (traces != null && traces.length > 0) {
            for (int i = 0; i < traces.length; i++) {
                builder.append("Trace[").append(i).append("]: ").append(traces[i]).append('\n');
            }
        }
        var suppress = ex.getSuppressed();
        if (suppress != null && suppress.length > 0) {
            for (int i = 0; i < suppress.length; i++) {
                builder.append("Suppressed[").append(i).append("]: ").append(suppress[i]).append('\n');
            }
        }
        if (hasChildException) {
            builder.append("---------- CAUSE EXCEPTION ----------").append('\n');
            readAll(builder, ex.getCause());
        }
    }

    public static void readAllMessages(StringBuilder builder, Throwable ex) {
        boolean hasChildException = hasChildException(ex);
        builder.append(ex.getMessage()).append("; \n");
        if (hasChildException) {
            readAllMessages(builder, ex.getCause());
        }
    }

    public static String readAllMessages(Throwable ex) {
        var builder = new StringBuilder(300);
        readAllMessages(builder, ex);
        return builder.toString();
    }

    public static String readAll(Throwable ex) {
        var builder = new StringBuilder(1000);
        readAll(builder, ex);
        return builder.toString();
    }

    public void printException() {
        log.info(readAll(this));
    }

    public String readAll() {
        return readAll(this.getCause());
    }

    public String readAllMessages() {
        return readAllMessages(this);
    }
}
