package ec.krugercorp.hiberus.infrastructure.controller;

import ec.krugercorp.hiberus.application.service.SuperHeroService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ec.krugercorp.hiberus.domain.dto.superheroes.*;

import java.util.List;

@Slf4j
@Api(value = "Super Heroes, Descripción: Estos endpoints son un ejemplo crud.")
@RestController
@RequestMapping("/api/v1/superheroes")
public class SuperHeroController {

    private final SuperHeroService superHeroService;

    @Autowired
    public SuperHeroController(final SuperHeroService superHeroService) {
        this.superHeroService = superHeroService;
    }

    @GetMapping()
    public ResponseEntity<List<SuperHeroDetail>> getAll() {
        return ResponseEntity.ok().body(superHeroService.getAll());
    }

    @PostMapping("/find")
    public ResponseEntity<List<SuperHeroDetail>> find(@RequestBody SuperHeroFilterRequest request) {
        return ResponseEntity.ok().body(superHeroService.find(request));
    }

    @GetMapping("{id}")
    public ResponseEntity<SuperHeroDetail> get(@PathVariable String id) {
        return ResponseEntity.ok().body(superHeroService.get(id));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ResponseEntity<String> create(@RequestBody SuperHeroCreateRequest request){
        return ResponseEntity.status(HttpStatus.CREATED).body(superHeroService.create(request));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping
    public ResponseEntity<Object> edit(@RequestBody SuperHeroEditRequest request){
        superHeroService.edit(request);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> delete(@PathVariable String id){
        superHeroService.delete(id);
        return ResponseEntity.ok().build();
    }
}