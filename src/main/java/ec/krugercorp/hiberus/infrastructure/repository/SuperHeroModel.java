package ec.krugercorp.hiberus.infrastructure.repository;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "super_heroes")
public class SuperHeroModel {
    @Id
    private String id;
    @Column
    private String alias;
    @Column
    private String first_name;
    @Column
    private String last_name;
    @Column
    private String skills;
    @Column
    private String gender;
    @Column
    private String date_of_birth;
    @Column
    private int status;
    @Column
    private String created_date;
    @Column
    private String modified_date;
    @Column
    private String deleted_date;
}
